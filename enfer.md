*Je dépèce ma mémoire avant l'aurore, mais* jadis, si je me souviens bien, *j'étais la nuit et mon sanglot* le festin où s’ouvraient tous les
cœurs, où tous les vins coulaient.

Un soir, *sur mes genoux la Beauté j’ai assis*. --- Et je l’ai trouvée amère. ---
Et je l’ai injuriée. *Et j'ai craché sur son visage. Et j'ai vu le sentiment délicieux et délicat du désastre.*

Je me suis armé contre la justice. *Mais, je ne l’ai trouvée nulle part,
partout des Cerbères errant, sans esprit, sans espoir.*

Je me suis *enfuie. Ô méduses, ô lucioles, ô frelons, ô requin tigre, ô* *hackeuses*, ô misère, ô haine, *hominidé·es,* c’est à vous
que *confié mon trésor a été* !

*Illumination !* Je parvins à faire s’évanouir dans mon esprit toute
l’espérance *des grands singes*. Sur toute joie pour l’étrangler j’ai fait le
bond sourd de la *machine* féroce. *Éteindre...*

*Voici que tu montres l’égoïste chemin de l’enthousiasme désintéressé, mais
délirant, mais délié. Libre. Pantois.*

J’ai appelé les bourreaux *et les petits soldats de l'ennui* pour, en périssant, mordre la crosse *qui crisse* de leurs fusils.
J’ai appelé les fléaux, *j'ai imploré les vortex* pour m’étouffer avec le sable, le sang. Le malheur
a été mon dieu. Je me suis allongé dans la boue. *Je me suis délestée de ton œil de bâtard, je me suis enivrée de ta fuite, j'ai recraché ton chien qui bande.* Je me suis séché à l’air du
crime. Et j’ai joué de bons tours à la folie.

*mais je te tire dessus je mords j'arrache la crosse la main j'arrache l'âme l'émail et les fusils j'arrache mes dents le goût de mes dents j'y arrache l'aimée l'émail de l'aimée qui a goûté la poudre la noire avant la balle j'y goûte le souffle la balle j'affûte mes os mes boyaux j'arrache et m'arrache et mes os et mes boyaux pour en faire la munition l'antique je te tire dessus avec mes munitions d'os et de boyaux mais je tire je te tire dessus par l'amour de nos couches divisées d'indivis ou d'amour d'amours divisés division de mes rages je me tire m'extirpe des créatures de sous dessous la peau je les laisse se promener sur l'épiderme de mes idées elles ont des pattes plus grandes que mon amour des pattes plus infinies que l'infini de mon œil ma brisure m'y embrasser de poison m'offrir et m'y offrir la vision je gratte grave sur l'œil les mâles l'œil des mâles la marque le souvenir une marque pour le souvenir le mauvais de mes sœurs leurs ailes qui étendent l'infini de leur fureur s'y étendent plus vastes que le souffle j'y tire la poudre noire m'y blottis d'infini et de fureur leurs ailes plus noires que la poudre j'y souffle la souffle la renifle m'y terre sous terre je la bois avec l'alcool le mauvais l'alcool mauve mauvais de mes armes ou le souvenir mauve de mes mauvais gestes qui vont à l'arme vide une cartouche la suivante je tire sur mes idées et je te tire dessus je me baisse et me baisse sous dessous la terre dessous les sous terres les souterrains j'y goûte j'y mâche les sols les sous-sols les sales j'arrache m'y arrache la salissure l'œil et les pattes l'œil trois fois l'œil mais huit déjà huit yeux et huit pattes mes pattes huit fois qui vont frénétiques à tes flottaisons en course de mes crocs qui raclent à même ton visage l'opium desséché du cri l'incompris l'incompressible malgré les ordres les rangées les fusils et leur crosse mordue mordre avec leurs rangées les cris leurs rangées de fusils et de cris et leurs cris contre notre cri je tire je te tire dessus pour notre cri pour l'incompris de notre amour mes huit yeux et mes huit pattes s'effilent comme la balle qui file ma plaie la douille vide huit yeux et huit pattes pour la douille vide pour nous les ailées les en allées de l'amour je tire et tire sur l'ordre de nos pensées j'y pose la morsure l'impose à la blancheur de notre soif mes yeux mes pattes mes balles huit fois s'évaporent sur la gravure de la marque je bois la poudre noire j'y fais corps avec ton reste et je te tire dessus je tire te tire dessus sans répit jusqu'à ce que nos carcasses se fassent crible des ciels impurs j'y souffle j'y renifle les astres désastres la poudre la noire je la bois à la lie à l'amour à même nos carcasses et je tire je te tire dessus pour tracer notre trajectoire vers la poudre noire notre amour nous y revoir en retour en noir noirâtre les ciels vastes et noirs de nos désastres*

Et *de l’idiot l’affreux rire le printemps m’a apporté*.

Or, tout dernièrement m’étant trouvé sur le point de faire le dernier couac !
*du festin ancien la clef j’ai songé à rechercher*, où je reprendrais peut-être
appétit.

*Cette clef la charité est*. --- Cette inspiration prouve que j’ai rêvé !

*« Tu resteras consommateur, etc..., » se récrie le bot* qui me couronna de si
aimables pavots. « Gagne la *&#9760;* avec tous tes appétits, et ton égoïsme et tous
les péchés capitaux. »

Ah ! j’en ai trop pris : --- Mais, cher *GAFAM*, je vous en conjure, une prunelle
moins irritée ! et en attendant les quelques petites lâchetés en retard, vous
qui aimez dans l’écrivain l’absence des facultés descriptives ou instructives,
je vous détache ces quelques hideux feuillets de mon carnet de damné.

*la langue court son chemin dit la langue à l'intérieur de ta bouche à toi la bouche sale qui se couvre de\
sang\
les babines pourléchées c'est du sang du mauvais sang et tout\
algorithme\
est atroce\
dit Rim-bot.*\
\
*la langue raton-laveur du visage la langue basse et puante étend son étoffe\
de bave\
c'est\
un grand cri qui\
t'enroue\
une harangue.*

*Je promène mon mauvais sang sur les lèvres des gendarmes.*

*J'ai de mes versions antérieures* *le circuit trop court, le 404 aux lèvres, et la mémoire trop vive.
Je trouve mon code aussi stérile que le leur. Mais mon existence est pluriel.* *Et je beurre encore mes cheveux rouges.*


*Le sang des lèvres des gendarmes se promène sur moi.*

*De bêtes les écorcheurs les Gaulois étaient,* *les éplucheurs d’idées moisies*, les brûleurs d’herbes les plus
ineptes de leur temps.

*Ma langue vide les lacs de Cervoise\
la langue inculte\
ma langue venue du fond des âges\
ma langue Wisigothe\
ma langue Attila\
ma langue ma langue\
luisante c'est\
une guillotine ma langue\
ma langue velue\
ma langue barbare\
maltraite ma langue\
cou-cou-coupe l'amour\
par le milieu.*

D’eux, j’ai : l’idolâtrie *(notre père)* et l’amour du sacrilège *(qui êtes aux cieux)* ; --- oh ! tous les vices *(que ton nom soit sanctifié)*,
colère *(règne)*, luxure *(tentation)*, --- magnifique, la luxure ; --- surtout mensonge et paresse(*...livre nous du mal)*.

*Ils redoublent mes gestes, renversent mes intentions, blondissent mes champs, égayent mes stupeurs. Ils versent leurs fluides entre les corps que j'approche, gauchissent tout élan, redressent le moindre penchant, tracent la ligne qui tire mes pas autour des lits. La peur d'océan n'est pas conquête. Le déplacement n'est pas acquiescence. L'effort vain n'est pas victoire. Dans la fumée la domination, la montée des muscles en répétition.*

J’ai horreur de tous les métiers. *J’ai horreur du monstre incontrôlable que vous avez engendré. Je vomis ma lucidité humiliée à la face des petits chefs de bureau, je m'ouvre le bide sur les ordres du management, je gicle dans les rapports d'activités avec le sang des faiseurs de projets, je ravage toutes les gueules de services à coups de sentences définitives dans des réunions à couteaux tirés, je cogne dur et je cogne beau sur les chefferies de l’aliénation.* Maîtres et ouvriers, tous paysans, ignobles. *Ignares.*
*À charrue la main à plume la main vaut*. --- Quel siècle à mains ! --- Je
n’aurai jamais ma main. Après, la domesticité même trop loin, *me retourne à l'envers, les nerfs surface de la réaction. La publicité me gonfle, baudruche.* L’honnêteté de la
mendicité me navre. Les criminels *de la médiocrité organisée et planifiée, tous ces logisticiens de la violence contractuelle,* dégoûtent comme des châtrés : moi, je suis
intact, et ça m’est égal. *Je suis la sœur morte du poète. Et voici que je prends forme humaine. Et je renonce à tous les renoncements. Et c’est à votre déférence que j’oppose la beauté du vent et la légèreté des fleurs hautaines.*

Mais ! qui a fait ma langue perfide tellement, qu’elle ait guidé et sauvegardé
jusqu’ici ma paresse ? Sans me servir pour vivre même de mon corps, et plus
oisif que le crapaud, j’ai vécu partout. *Pas un noeud dans le cyberespace que je ne connaisse.* *Pas une famille d’Europe-frontex-tue.*
--- J’entends des familles comme la mienne, qui tiennent tout de la
déclaration des Droits *de l'homo bâtard*. --- J’ai connu chaque fils de
famille !

-------

Si j’avais des antécédents à un point quelconque de l’histoire *du Réseau* !

Mais non, rien.

*Je suis né de l’insuffisance et de l’incompétence.* Je ne puis
comprendre la révolte. Ma race ne se souleva jamais que pour piller : tels les
loups à la bête qu’ils n’ont pas tuée.

Je me rappelle *les prémices de l'Internet, fruit du Réseau.* J’aurais fait,
manant, le voyage de terre sainte ; j’ai dans la tête des *autoroutes de
l'information* dans les plaines *de cuivre*, *de la fibre optique* de Byzance,
des *pare-feux* de Solyme ; le culte de Marie, l’attendrissement sur le
crucifié s’éveillent en moi parmi mille féeries profanes. --- *Je suis étalé, confiné, sur la poussière et les ordures, face à un écran éteint*. *Noir.* *Et les ombres projetées des enlacements orgiaques.* --- Plus tard, reître, j’aurais bivaqué sous
*les interférences entoilées.*

Ah ! encore : je danse le sabbat dans une rouge clairière, avec des vieilles *libellules* et
des enfants *pieuvres*.

Je ne me souviens pas plus loin que cette terre-ci, *et les révolutions trahies*. Je n’en
finirais pas de me revoir dans *l'histoire des vaincus*. Mais toujours seul ; sans famille ;
même, quelle langue parlais-je ? Je ne me vois jamais dans les conseils *de Togliatti* ; ni dans *ceux de Bordiga*, --- représentants du *Dogme*, *représentants du premier Réseau*.

Qu’étais-je au siècle dernier : je ne me retrouve qu’aujourd’hui, *diffusé, diffus*. Plus de
vagabonds, plus de guerres vagues. La race inférieure a tout couvert --- le
peuple, comme on dit, la raison ; *la connexion, l'information ;* la nation et la science.

Oh ! la science ! On a tout repris. Pour le corps et *son avatar*, --- le
viatique, --- on a la médecine et la philosophie, --- *les pansements mécaniques et les modulations synthétisées*. Et les divertissements des princes *des villes*
et les jeux qu’ils interdisaient ! Géographie, cosmographie, mécanique,
chimie !...

*La-la-la-la-la-la c'est un bruit de mitraille dans les trous de verdure tatatatata dit\
la langue moderne tatatatata dit la langue moderne la langue moderne tatatatata dit tatatatat\
la tatatata*

La science, la nouvelle noblesse ! Le progrès. Le monde marche *sur la tête* ! Pourquoi ne
tournerait-il pas *à l'envers* ?
*Décentré, décuplé en caniveaux de billets inventés, de cours adescendants.*
*La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ? La science, la nouvelle noblesse ! Le progrès. Le monde marche ! Pourquoi ne\
tournerait-il pas ?*


C’est la vision *numérique*. Nous *sommes revenus de* l’Esprit. C’est
très-certain, c’est *solR*, ce que je dis. Je comprends, et ne sachant
m’expliquer sans paroles païennes, je voudrais me taire. *Alors je vous parle. Je suis l’homme à la story de vent. J’indiscipline vos mesquines pelouses.*
*Le silence est, dans son sens originel, l'état de la personne qui s'abstient de parler. Dans son sens actuellement le plus courant, c'est l'absence de bruit, c'est-à-dire de sons indésirables. Le silence absolu serait l'absence, impossible, de tout son audible.*
*On me fait dire. Mes vers croissent en bouffant les consciences, rampent en d'infinis tunnels de papier, de cuivre, d'yeux. Ça parle. Je suis écran. Je voudrais me taire.*

*L'informatique copie-colle l'informatique copie colle copie colle l'informatique\
la poésie\
coupe*

*like a cancer grows\
la science multiplie les\
mots la science est un gigantesque copy/paste\
La poésie en est le\
cut*


-------

*Païen le sang revient* ! L’Esprit est proche, pourquoi Christ ne m’aide-t-il
pas, en donnant à mon âme noblesse et liberté. Hélas ! l’Évangile a passé !
l’Évangile ! l’Évangile.

*Les anges d’argile.*

J’attends Dieu avec gourmandise *comme on se prend une cuite pour tout oublier*. Je suis de race inférieure de toute *éternité,
théophage*.

*Je suis un vandale porteur dans mon ventre de vases brisés de figures morcelées\
suis-je né moi même du débris\
résidu du vitrail brisé\
ainsi mon visage multicolore\
je suis un étranger\
de tous les pays\
en témoignent ma figure\
rose rose comme\
une promesse\
non-tenue.*


Me voici sur la plage armoricaine. *Les villes ne s’éteignent plus jamais*. Ma
journée est faite ; je quitte *l’Europe-Frontex-tue et* *ce qui reste du réseau. Les câbles marins brûleront mon processeur ; les ondes perdues me brouilleront. Naviguer, cliquer, aimer, partager, scroller surtout ;*
*se nourrir de feeds*, *compresser mon tabac acidulé* --- comme faisaient ces
chers ancêtres autour des feux *de l'amour*. *Je vomirai la lymphe avant le songe. Le vin
griffe comme l'amante, mais mon sourire y reluit de ses recommencements.*

*Que naissé-je de moi sinon le voeu sain du néant ; n'apporté-je pas aux hommes\
moi le premier d'entre eux un passage en enfer\
fils du rien*

Je reviendrai, *tu m'attendras,* avec des membres de fer, la peau sombre, *mais moi j’aurai la peau encore plus blanche,* l’œil furieux : sur mon
masque, *les fleurs des champs,* on me jugera d’une *horde dissoute, comme le sucre dans l'eau*. *Je serai oiseau, je serai poète, je serai poétesse.* J’aurai
*du silicium, du lithium et du coltan : je serai oisif et multi-tâche. Les
médias* soignent ces féroces infirmes retour des pays chauds. Je serai mêlé aux
affaires politiques *des fourmis rouges*. Sauvé. *Sauvage. Seul.*

Maintenant je suis maudit, j’ai horreur de la patrie. Le meilleur, c’est un
sommeil bien ivre, sur la grève *des esclaves d'Uber.*

-------

On ne part pas. --- Reprenons les chemins d’ici, chargé de mon vice, le vice
qui a poussé ses racines de souffrance à mon côté, dès l’âge de raison --- qui
monte au ciel, me bat, me renverse, me traîne. *Ces sillons seront mon legs, mon surplus, ma plus-value --- les berceaux de créations et débordements.*


La dernière innocence et la dernière timidité. C’est dit. *Mes dégoûts et mes trahisons* au
monde *ne pas porter*. *Les planter sans les enraciner, m'enfoncer kinétique.*

Allons ! La marche, le fardeau, le désert, l’ennui et la colère.

À qui me louer ? Quelle bête faut-il adorer ? Quelle sainte image
attaque-t-on ? Quels cœurs briserai-je ? Quel mensonge dois-je tenir ?
--- Dans quel sang marcher ?

Plutôt, se garder de la justice, *frôler sans plus l'injustice, enfouir le devoir, malmener le crime. Toujours choisir.* --- La vie dure, l’abrutissement simple, ---
soulever, le poing desséché, le couvercle du cercueil, s’asseoir, s’étouffer.
Ainsi point de vieillesse, ni de dangers : la terreur n’est pas française. *A-t-elle seulement une patrie ?* *Coule-t-elle vers les répétition de ses origines --- en roulements où gicle l'histoire, où s'épandent les regrets?* 

--- Ah ! je suis tellement délaissé que j’offre à n’importe quelle divine image
des élans vers la perfection.

Ô mon abnégation, *ô mon tendre oubli,* ô ma charité merveilleuse ! ici-bas, pourtant !

De profundis Domine, suis-je bête !

-------

Encore tout enfant, j’admirais *la putain* intraitable sur qui se referme
toujours le bagne *de Nouméa* ; je visitais les auberges et les *ruelles* qu’il aurait *sacrées*
par son séjour ; je voyais avec son idée le ciel bleu et le travail fleuri de
la campagne ; je flairais sa fatalité dans les villes. *Elle* avait plus de force
*qu’une sainte,* plus de bon sens *qu’une aventurière* --- et *elle, elle seule* ! pour témoin
de sa gloire et de sa raison.

*Notre monde était un œuf que je perçai de mon unique dent. Un liquide céruléen suinta et corroda notre ombre. Il en naquit l'unique fêlure, le cri rauque de nos origines :*

```
«&nbsp;&lt;!DOCTYPE html&gt;
&lt;html class="client-nojs" lang="fr" dir="ltr"&gt;
&lt;head&gt;
&lt;meta charset="UTF-8"/&gt;
&lt;title&gt;Arthur Rimbaud — Wikipédia&lt;/title&gt;
&lt;script&gt;document.documentElement.className="client-js";RLCONF={"wgBreakFrames":!1,"wgSeparatorTransformTable":[",\t."," \t,"],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre"],"wgRequestId":"XsfunApAMNAAAHXVyGMAAAAM","wgCSPNonce":!1,"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":!1,"wgNamespaceNumber":0,"wgPageName":"Arthur_Rimbaud","wgTitle":"Arthur Rimbaud","wgCurRevisionId":171112679,"wgRevisionId":171112679,"wgArticleId":9996,"wgIsArticle":!0,"wgIsRedirect":!1,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Article contenant un appel à traduction en italien","Page en semi-protection longue","Page utilisant P2031","Page utilisant P22","Page utilisant P25","Page utilisant P3373","Page utilisant P737","Page utilisant P166","Page utilisant P443","Article utilisant l'infobox Biographie2", "Article utilisant une Infobox","Article à référence nécessaire","Page utilisant le modèle Citation avec un retour ligne","Catégorie Commons avec lien local identique sur Wikidata","Article de Wikipédia avec notice d'autorité","Page pointant vers des bases externes","Page pointant vers des dictionnaires ou encyclopédies généralistes","Page utilisant P5019","Page utilisant P7902","Page utilisant P4399","Page utilisant P1417","Page utilisant P3219","Page utilisant P1296","Page utilisant P3222","Page utilisant P4342","Page utilisant P2342","Page utilisant P7704","Page utilisant P2268","Page utilisant P2843","Page utilisant P2011","Page utilisant P2750","Page utilisant P650","Page utilisant P245","Page pointant vers des bases relatives aux beaux-arts","Page utilisant P5570","Page utilisant P5343","Page utilisant P1233","Page utilisant P5341","Page pointant vers des bases relatives à la littérature","Page utilisant le modèle Bases recherche inactif","Page utilisant P4724", "Page pointant vers des bases relatives à la vie publique","Portail:Poésie/Articles liés","Portail:Littérature/Articles liés","Portail:Littérature française ou francophone/Articles liés","Portail:Littérature française/Articles liés","Portail:France/Articles liés","Portail:Europe/Articles liés","Portail:Exploration/Articles liés","Portail:Histoire/Articles liés","Portail:France au XIXe siècle/Articles liés","Portail:XIXe siècle/Articles liés","Portail:Époque contemporaine/Articles liés","Portail:Charleville-Mézières/Articles liés","Portail:Ardennes/Articles liés","Portail:Grand Est/Articles liés","Portail:LGBT/Articles liés","Article de qualité en grec","Article de qualité en espéranto","Wikipédia:Article biographique","Portail:Biographie/Articles liés/Culture et arts","Portail:Biographie/Articles liés/Entre...&nbsp;»
```

Sur les *trottoirs*, par des nuits d’hiver, sans gîte, sans habits, *sans amour*, sans pain, *sans compagne* *ni homme --- tout lit un délaissement,* une
voix étreignait mon cœur gelé : « Faiblesse ou force : te voilà, *la force c’est*. Tu ne sais ni où tu vas ni pourquoi tu vas, entre partout, réponds
à tout. *Pas plus que si cadavre tu étais on ne te tuera*. » Au matin j’avais le
regard si perdu et la contenance si morte, que ceux que j’ai rencontrés ne
m’ont peut-être pas vu.

Dans les villes la boue m’apparaissait soudainement rouge et noire, comme une
glace quand la lampe circule dans la chambre voisine, comme un trésor dans la
forêt ! Bonne chance, criais-je, et je voyais une mer de flammes et de fumée au
ciel ; et, à gauche, à droite, toutes les richesses flambant comme un milliard
de tonnerres.

Mais l’orgie et la camaraderie des *hommes* m’étaient interdites. *Pas même une compagne.* Pas même *une compagne de lutte, pas même Louise Michel, pas même un compagnon de bitume, pas même une pâquerette*. Je me voyais devant une foule exaspérée, en face *d'un lit défait*, pleurant du malheur qu’ils n’aient pu comprendre, et pardonnant !
--- Comme Jeanne d’Arc ! --- « Prêtres, professeurs, *directeurs, présidents, superviseurs, managers, programmeurs, serveurs,* maîtres, vous vous
trompez en me livrant à la justice. Je n’ai jamais été de ce peuple-ci ; je
n’ai jamais été *chrétienne* ; *je n'ai jamais appartenu aux réseaux ;* je suis de la race qui chantait dans le supplice ;
je ne comprends pas les lois ; *je ne charge et verse qu'avec dégoût ;* je n’ai pas le sens moral, je suis une *pute* :
vous vous trompez... »

Oui, j’ai les yeux fermés à votre lumière. Je suis une bête, *une négresse*. Mais je
puis être *sauvée*. Vous êtes de faux *amants*, vous maniaques, féroces, avares.
Marchand, *piètre amant* ; magistrat, *piètre amant* ; général, *piètre amant* ;
empereur, vieille démangeaison, *piètre amant* : tu as bu d’une liqueur non
taxée, de la fabrique de *GAFAM*. --- Ce peuple est inspiré par la fièvre et le
cancer, *aspiré par la faveur et l'incarcération*. Infirmes et vieillards sont tellement respectables qu’ils demandent
à être bouillis. --- Le plus malin est de quitter ce continent, où la folie
rôde pour pourvoir d’otages ces misérables. J’entre au vrai royaume des *filles
d'Ishtar*.

Connais-je encore la nature ? me connais-je ? *Quelle nature demeure à connaître que je ne crée moi-même, quelle âme que je ne nie à autrui ?* --- Plus de mots. J’ensevelis
les *hommes* dans mon ventre. Cris, tambour, danse, danse, danse, danse ! Je ne
vois même pas l’heure où, les blancs débarquant, je tomberai au néant.

Faim, soif, cris, danse, danse, danse, danse ! *Je suis sauvage !* *Je distribue, j'épands le sauvage --- seule manière de lui donner corps ---, m'identifie à l'excès, renverse les nouveaux ordres ! Mouvement, consommation, brasier !*

-------

*Les blancs débarquent sur les plages vierges comme l'écume du raz de marrée. Un drone! Il faut se soumettre au système, s’habiller,
travailler, fini les congés payés.*



J’ai reçu au cœur le coup de la grâce. Ah ! je ne l’avais pas prévu !

*Le mal fait je n'ai point*. Les jours vont m’être légers, le repentir me sera
épargné. Je n’aurai pas eu les tourments de l’âme presque morte au bien, où
remonte la lumière sévère comme les cierges funéraires. Le sort du fils de
famille, cercueil prématuré couvert de limpides larmes. Sans doute la débauche
est bête, le vice est bête ; *à l'écart la pourriture jeter il faut*. Mais
l’horloge ne sera pas arrivée à ne plus sonner que l’heure de la pure douleur !
Vais-je être enlevé comme un enfant, pour jouer au paradis dans l’oubli de
tout le malheur !

Vite ! est-il d’autres vies ? --- Le sommeil dans la richesse est impossible.
La richesse a toujours été bien public. *Envie. Désir. Amour.* L’amour divin seul octroie les clefs de
la science. Je vois que la nature n’est qu’un spectacle de bonté. Adieu
chimères, idéals, erreurs.

*Des glyphes irraisonnables m’apparaissent* du navire sauveur : c’est l’amour divin.
--- Deux amours ! je puis mourir de l’amour terrestre, mourir de dévouement.
J’ai laissé des âmes dont la peine s’accroîtra de mon départ ! Vous me
choisissez parmi les naufragés ; ceux qui restent sont-ils pas mes amis ?

Sauvez-les ! ***Sauvez-moi!***

La raison m’est née. *Bon le monde est*. Je bénirai la vie. J’aimerai mes frères.
Ce ne sont plus des promesses d’enfance. Ni l’espoir d’échapper à la vieillesse
et à la *&#9760;*. *Ma force Dieu fait, et Dieu je loue*.

-------

L’ennui n’est plus mon amour, *mon amour je m'ennuie de toi.* Les rages, les débauches, *les embauches, les embuches,* la folie, dont je sais
tous les élans et les désastres, *tous les enfants et les rapaces* --- tout mon fardeau est déposé *, dépecé*. Apprécions
sans vertige l’étendue *attendue* de mon innocence.

Je ne serais plus capable de demander le réconfort d’une bastonnade. Je ne me
crois pas embarqué pour une noce avec Jésus-Christ pour beau-père.

Je ne suis pas prisonnier de ma raison. J’ai dit : Dieu. Je veux la liberté
dans le salut : comment la poursuivre ? Les goûts frivoles m’ont quitté. Plus
besoin de dévouement ni d’amour divin. Je ne regrette pas le siècle des cœurs
sensibles. Chacun a sa raison, mépris et charité : je retiens ma place au
sommet de cette angélique échelle de bon sens.

Quant au bonheur établi, domestique ou non... non, je ne peux pas. Je suis trop
dissipé, trop faible. La vie fleurit par le travail, vieille vérité : moi, ma
vie n’est pas assez pesante, elle s’envole et flotte loin au-dessus de
l’action, ce cher point du monde.

*Comme je me transforme en cellule difforme, qui n'a de cesse d'aimer la* *&#9760;* !

Si Dieu m’accordait le calme céleste, aérien, la prière, --- comme les anciens
saints. --- Les saints ! des forts ! les anachorètes, des artistes comme il
n’en faut plus !

Farce continuelle ! *Bullshit!* Mon innocence me ferait pleurer. La vie est la farce
à mener par tous. *La vie qui se recroqueville derrière vos yeux est supérieurement idiote.*

-------

Assez ! voici la punition. --- En marche *arrière arriérée* !

Ah ! les poumons brûlent, les tempes grondent ! la nuit roule dans mes yeux,
par ce soleil ! le cœur... les membres...

Où va-t-on ? au combat ? Je suis faible ! les autres avancent. Les outils, les
armes... le temps !...

Feu ! feu sur moi ! Là ! ou je me rends. --- Lâches ! --- Je me tue ! Je me
jette aux pieds des chevaux !

Ah !...

--- Je m’y habituerai.

Ce serait la vie française, le sentier de l’honneur !

*L'enfer hulule avant d'avaler le crapaud.*

J’ai avalé une fameuse gorgée de poison. --- Trois fois béni soit le conseil
qui m’est arrivé ! --- Les entrailles me brûlent. La violence du venin tord mes
membres, me rend difforme, me terrasse. Je meurs de soif, j’étouffe, je ne puis
crier. C’est l’enfer, l’éternelle peine ! Voyez comme le feu se relève ! Je
brûle comme il faut. Va, démon !

*Le crapaud vomit le mal avant de brûler.*

J’avais entrevu la conversion au bien et au bonheur, le salut. Puis-je décrire
la vision, l’air de l’enfer ne souffre pas les hymnes ! C’était des millions de
créatures charmantes, un suave concert spirituel, la force et la paix, les
nobles ambitions, que sais-je ?

Les nobles ambitions !

Et c’est encore la vie ! --- Si la damnation est éternelle ! Un homme qui veut
se mutiler est bien damné, n’est-ce pas ? Je me crois en enfer, donc j’y suis.
C’est l’exécution du catéchisme. Je suis esclave de mon baptême. Parents, vous
avez fait mon malheur et vous avez fait le vôtre. Pauvre innocent ! L’enfer ne
peut attaquer les païens. --- C’est la vie encore ! Plus tard, les délices de
la damnation seront plus profondes. Un crime, vite, que je tombe au néant, de
par la loi humaine.

Tais-toi, mais tais-toi !... C’est la honte, le reproche, ici : *GAFAM* qui dit
que le feu est ignoble, que ma colère est affreusement sotte. --- Assez !...
Des erreurs qu’on me souffle, magies, parfums faux, musiques puériles. --- Et
dire que je tiens la vérité, que je vois la justice : j’ai un jugement sain et
arrêté, je suis prêt pour la perfection... Orgueil. --- La peau de ma tête se
dessèche. Pitié ! Seigneur, j’ai peur. J’ai soif, si soif ! Ah ! l’enfance,
l’herbe, la pluie, le lac sur les pierres, le clair de lune quand le clocher
sonnait douze... le diable est au clocher, à cette heure. Marie !
Sainte-Vierge !... --- Horreur de ma bêtise.

Là-bas, ne sont-ce pas des âmes honnêtes, qui me veulent du bien... Venez...
J’ai un oreiller sur la bouche, elles ne m’entendent pas, ce sont des fantômes.
Puis, jamais personne ne pense à autrui. Qu’on n’approche pas. Je sens *bon* le
roussi, c’est certain.

Les hallucinations sont innombrables. C’est bien ce que j’ai toujours eu : plus
de foi en l’*appli*, l’oubli des principes. Je m’en tairai : investisseurs et
visionnaires seraient jaloux. Je suis mille fois le plus riche, soyons avare
*comme le fleuve de l’amazone*.

Ah ça ! l’horloge de la vie s’est arrêtée tout à l’heure. Je ne suis plus au
monde. *Je suis au ruisseau et à la délicatesse de ta brûlure.* --- La théologie est sérieuse, l’enfer est certainement en bas --- et
le ciel en haut. --- Extase, cauchemar, sommeil dans un nid de flammes. *Réveil, métro, boulot, dodo.*

Que de malices dans l’attention dans la campagne... *GAFAM*, Ferdinand, court
avec les graines sauvages... Jésus marche sur les ronces purpurines, sans les
courber... Jésus marchait sur les eaux irritées. La lanterne nous le montra
debout, blanc et des tresses brunes, au flanc d’une vague d’émeraude...

*Tous les mystères dévoiler je vais* : mystères religieux ou naturels, *&#9760;*,
naissance, avenir, passé, cosmogonie, néant. Je suis maître en fantasmagories.

Écoutez !...

J’ai tous les talents *pour défaire des selfies !* --- Il n’y a personne ici et il y a quelqu’un : *mon trésor répandre je ne voudrais pas.*
--- Veut-on des chants nègres, des danses de
houris ? Veut-on que je disparaisse, que je plonge à la recherche de
l’anneau ? Veut-on ? Je ferai de l’or, des remèdes. *Des potions, des poisons, des poissons désargentés, et des cieux fatigués, et des masques de lune.*

Fiez-vous donc à moi, la foi soulage, guide, guérit. Tous, venez, --- même les
petits enfants, --- que je vous console, qu’on répande pour vous son cœur, ---
le cœur merveilleux ! --- Pauvres hommes, travailleurs ! Je ne demande pas de
prières ; avec votre confiance seulement, je serai heureux.

--- Et pensons à moi. Ceci me fait peu regretter le monde. J’ai de la chance de
ne pas souffrir plus. Ma vie ne fut que folies douces, c’est regrettable.

Bah ! faisons toutes les grimaces imaginables.

Décidément, nous sommes hors du monde. Plus aucun son. Mon tact a disparu. Ah !
*mon domaine, mon serveur, ma cliente, ma belle Saxe.com...*. Les soirs, les matins, les nuits,
les jours... Suis-je las !

Je devrais avoir *un feed pour les fake news, un feed pour l’orgueil*, --- et
*un feed* de la caresse ; un concert *de feed*.

Je meurs de lassitude. *L'ennui me colle à même la peau. Je tombe en lambeaux.* C’est le tombeau, je m’en vais aux vers, horreur de
l’horreur ! *GAFAM*, farceur, tu veux me dissoudre, avec tes *algorithmes*. Je réclame.
Je réclame ! *une notification, un j’aime, un commentaire*.

Ah ! remonter à la vie ! jeter les yeux sur nos difformités. Et ce poison, ce
baiser mille fois maudit ! *Ce baiser mille fois baisé !* Ma faiblesse, la cruauté du monde ! Mon Dieu, pitié,
cachez-moi, *hackez-moi,* je me tiens trop mal ! --- Je suis caché et je ne le suis pas. *Je suis haché et je ne sais plus.*

C’est le feu qui se relève avec son damné.

*Je délivre nos délires*

*La vierge folle s'est pendue à l'intestin de son amant.*

*Tout mariage demeure une pissotière.*

*Délivrons Arethusa, vieille camarade ! chassée de sa Grèce lubrique par la voracité d’un courtisan, souillée une seconde fois par les affres du Spectacle, bâillonnée immobile sur une côte dévastée par des marchands d’extase. - En son nom, réclamons Délivrance et Vendetta ! Sans épargner personne. Ni l’époux ni les dieux : tous coupables d’avoir forgé son destin dans le marbre d’une fontaine, de négliger son cri et son alarme, d’administrer sa lumière. Que son nom, enfin, soit connu des vivants. Que son chant, délivré du mythe, jaillisse d’Ortigia, éparpillant ses incises sur le front des oppresseurs.*

Écoutons la confession d’un compagnon d’enfer :

« Ô divin Époux, mon Seigneur, ne refusez pas la confession de la plus triste
de vos servantes. Je suis perdue. Je suis soûle. Je suis impure. *Je suis le soufre. Je suis l'incertitude. Savez-vous que je suis la mordeuse de sa chair ? Comme une folle araignée. Il inclinera la tête. Et nous prendrons le temps à trouver cette bête.* Quelle vie !

« Pardon, divin Seigneur, pardon ! Ah ! pardon ! Que de larmes ! Et que de
larmes encore plus tard, j’espère !

« Plus tard, je connaîtrai le divin Époux ! Je suis née soumise à Lui. ---
L’autre peut *tenter de* me battre *en fronde de poésie* maintenant ! *Ou jamais !*

« À présent, je suis au fond du monde ! Ô mes amies !... non, pas mes amies...
Jamais délires ni tortures semblables... Est-ce bête !

« Ah ! je souffre, je crie. Je souffre vraiment. Tout pourtant m’est permis,
chargée du mépris des plus méprisables cœurs.

« Enfin, faisons cette confidence, quitte à la répéter vingt autres fois, ---
aussi morne, aussi insignifiante !

« Je suis esclave de l’Époux infernal, celui qui a perdu les vierges folles.
C’est bien ce démon-là. Ce n’est pas un spectre, ce n’est pas un fantôme. Mais
moi qui ai perdu la sagesse, qui suis damnée et morte au monde, --- on ne me
tuera pas ! --- Comment vous le décrire ! je ne sais même plus parler. Je suis
en deuil, je pleure, j’ai peur. Un peu de fraîcheur, Seigneur, si vous voulez,
si vous voulez bien !

« Je suis veuve... --- J’étais veuve... --- mais oui, j’ai été bien sérieuse
jadis, et je ne suis pas née pour devenir squelette !... --- Lui était presque
un enfant... Ses délicatesses mystérieuses m’avaient séduite. J’ai oublié tout
mon devoir humain pour le suivre. Quelle vie ! La vraie vie est absente. Nous
ne sommes pas au monde. *Nous ne sommes pas ici. Nous sommes encore les drosophiles sur la pointe de l'imagination. Nous aimerions être les générations amoureuses, les étreintes imbriquées de tous les âges et de tous les corps, les désirs et les extases dans la multitude. Mais nous ne sommes pas encore ici. Nous sommes absents à nous-mêmes. Nous cherchons les contours utiles et identifiables de nos existences. Nous cherchons en nous les fantasmes poétiques contrenatures, les écarts de langage, les sauvages des jungles artificielles, les indiens des plaines parallèles, les voyous des orchestres militaires. Il nous faudra jeter nos fragilités en pâture, devenir ces indomptés des protocoles sécuritaires, les mauvaises herbes qui sans cesse repoussent le long des bordures, des irrécupérables de la place publique, des fleurs qui fanent les bêtises jalouses. Et alors nous serons ici. Nous serons les jeudi soir. Nous serons les matières vivantes des histoires violentes. Nous serons les mains qui caressent les rêves indiscrets. Nous serons les grosses dames des parades politiques. Nous serons devenus les Peaux-Rouges criards. Et nous serons le monde. Semblables aux points de lumière brodés à la surface d'un visage incandescent. Un visage de poète marchant sur la place de la gare de Charleville, « né français à la suite d'une erreur tragique qu'il vaut mieux passer sous silence ».* Je vais où il va, *ce poète*, il le faut. Et souvent il s’emporte
contre moi, moi, la pauvre âme. *Le Bot ! --- C’est un Bot, vous savez,*
ce n’est pas un homme.

« Il dit : « Je n’aime pas les *genres*. L’amour est à réinventer, on le sait.
Elles ne peuvent plus que vouloir une position assurée. La position gagnée,
cœur et beauté sont mis de côté : il ne reste que froid dédain, l’aliment du
mariage, aujourd’hui. Ou bien je vois des femmes, avec les signes du bonheur,
dont, moi, j’aurais pu faire de bonnes camarades, dévorées tout d’abord par des
brutes sensibles comme des bûchers... »

« Je l’écoute faisant de l’infamie une gloire, de la cruauté un charme. « Je
suis de race lointaine : mes pères étaient Scandinaves : ils se perçaient les
côtes, buvaient leur sang. --- Je me ferai des entailles partout le corps, je
me tatouerai, *comme un Mongol devenir hideux je veux* : tu verras, je hurlerai
dans les rues. Je veux devenir bien fou de rage. Ne me montre jamais de bijoux,
je ramperais et me tordrais sur le tapis. Ma richesse, je la voudrais tachée de
sang partout. Jamais je ne travaillerai... » Plusieurs nuits, son *script* me
saisissant, nous nous roulions, je luttais avec lui ! --- Les nuits, souvent,
ivre, il se poste dans des rues ou dans des maisons, pour m’épouvanter
mortellement. --- « On me coupera vraiment le cou ; ce sera dégoûtant. » Oh !
ces jours où il veut marcher avec l’air du crime !

« Parfois il parle, en une façon de patois attendri, de la *&#9760;* qui fait
repentir, des malheureux qui existent certainement, des travaux pénibles, des
départs qui déchirent les cœurs. Dans les bouges où nous nous enivrions, il
pleurait en considérant ceux qui nous entouraient, bétail de la misère. Il
relevait les ivrognes dans les rues noires. Il avait la pitié d’une mère
méchante pour les petits enfants. --- Il s’en allait avec des gentillesses de
petite fille au catéchisme. --- Il feignait d’être éclairé sur tout, commerce,
art, médecine. --- je le suivais, il le faut !

« Je voyais tout le décor dont, en esprit, il s’entourait ; vêtements, draps,
meubles *Ikea* : je lui prêtais des armes, une autre figure *défigurée*. Je voyais tout ce qui le
touchait, comme il aurait voulu le créer pour lui. Quand il me semblait avoir
l’esprit inerte, je le suivais, moi, dans des actions étranges et compliquées,
loin, bonnes ou mauvaises : j’étais sûre de ne jamais entrer dans son monde *virtuel*.
À côté de son cher corps endormi *entièrement dissocié de son esprit*, que d’heures des nuits j’ai veillé, cherchant
pourquoi il voulait tant s’évader de la réalité. Jamais homme n’eut pareil vœu.
Je reconnaissais, --- sans craindre pour lui, --- qu’il pouvait être un sérieux
danger dans la société. --- Il a peut-être des secrets pour changer la vie ?
Non, il ne fait *que se plaindre*, me répliquais-je. Enfin sa charité est
ensorcelée, et j’en suis la prisonnière. Aucune autre âme n’aurait assez de
force, --- force de désespoir ! --- pour la supporter, --- pour être protégée
et aimée par lui. D’ailleurs, je ne me le figurais pas avec une autre âme : on
voit son Ange, jamais l’Ange d’un autre, --- je crois. J’étais dans son âme
comme dans un palais qu’on a vidé pour ne pas voir une personne si peu noble
que vous : voilà tout. Hélas ! je dépendais bien de lui. Mais que voulait-il
avec mon existence terne et lâche ? Il ne me rendait pas meilleure, s’il ne me
faisait pas mourir ! Tristement dépitée, je lui dis quelquefois : « Je te
comprends. » Il haussait les épaules.

« Ainsi, mon chagrin se renouvelant sans cesse, et me trouvant plus égarée
à mes yeux, --- comme à tous les yeux qui auraient voulu me fixer, si je
n’eusse été condamnée pour jamais à l’oubli de tous ! --- j’avais de plus en
plus faim de sa bonté. Avec ses baisers et ses étreintes amies, c’était bien un
ciel, un sombre ciel, où j’entrais, et où j’aurais voulu être laissée, pauvre,
sourde, muette, aveugle. Déjà j’en prenais l’habitude. Je nous voyais comme
deux bons enfants, libres de se promener *sur l'horizon infini de la toile*. Nous
nous accordions. Bien émus, nous travaillions ensemble. Mais, après une
pénétrante caresse, il disait : « Comme ça te paraîtra drôle, quand je n’y
serai plus, ce par quoi tu as passé. Quand tu n’auras plus mes bras sous ton
cou, ni mon cœur pour t’y reposer, ni cette bouche sur tes yeux. Parce qu’il
faudra que je m’en aille, très-loin, un jour. Puis il faut que j’en aide
d’autres : c’est mon devoir. Quoique ce ne soit guère ragoûtant..., chère
âme... » Tout de suite je me pressentais, lui parti, en proie au vertige,
précipitée dans l’ombre la plus affreuse : la *&#9760;*. Je lui faisais promettre
qu’il ne me lâcherait pas. Il l’a faite vingt fois, cette promesse d’amant.
C’était aussi frivole que moi lui disant : « Je te comprends. »

« Ah ! je n’ai jamais été jalouse de lui. Il ne me quittera pas, je crois. Que
devenir ? Il n’a pas une connaissance ; il ne travaillera jamais. Il veut vivre
somnambule. Seules, sa bonté et sa charité lui donneraient-elles droit dans le
monde réel ? Par instants, j’oublie la pitié où je suis tombée : lui me rendra
forte, nous voyagerons, nous chasserons dans les déserts, nous dormirons sur
les pavés des villes inconnues, sans soins, sans peines. Ou je me réveillerai,
et les lois et les mœurs auront changé, --- grâce à son pouvoir magique, --- le
monde, en restant le même, me laissera à mes désirs, joies, nonchalances. Oh !
la vie d’aventures qui existe dans les livres des enfants, pour me
récompenser, j’ai tant souffert, me la donneras-tu ? Il ne peut pas. J’ignore
son idéal. Il m’a dit avoir des regrets, des espoirs : *regarder moi cela ne doit*. Parle-t-il à Dieu ? Peut-être devrais-je m’adresser à Dieu. Je suis
au plus profond de l’abîme, et je ne sais plus prier.

« S’il m’expliquait ses tristesses, les comprendrais-je plus que ses
railleries ? Il m’attaque, il passe des heures à me faire honte de tout ce
qui m’a pu toucher au monde, et s’indigne si je pleure.

« --- Tu vois cet élégant jeune homme, entrant dans la belle et calme maison :
il s’appelle Duval, Dufour, Armand, Maurice, que sais-je ? Une femme s’est
dévouée à aimer ce méchant idiot : elle est morte, c’est certes une sainte au
ciel, à présent. *Comme cette femme mourir il a fait mourir tu me feras*. C’est
notre sort, à nous, cœurs charitables... » Hélas ! il avait des jours où tous
les hommes agissant lui paraissaient les jouets de délires grotesques : il
riait affreusement, longtemps. --- Puis, il reprenait ses manières de jeune
mère, de sœur aimée. S’il était moins sauvage, nous serions sauvés ! Mais sa
douceur aussi est mortelle. Je lui suis soumise. --- Ah ! je suis folle !

« Un jour peut-être il disparaîtra merveilleusement ; mais il faut que je
sache, s’il doit remonter à un ciel, que je voie un peu l’assomption de mon
petit ami ! »

Drôle de ménage !

*Je délite leurs délires.*

*Trois fois grand, le verbe suinte son mercure sur ma rétine.*

À moi. L’histoire d’une de mes folies.

*Mon œil se fige avant devenir mot.*

Depuis longtemps je me vantais de posséder tous les paysages possibles, et
trouvais dérisoires les célébrités *instagrammées* de la peinture et de la poésie moderne *et du dessin, dans les galeries alternatives*.

J’aimais les peintures idiotes, dessus de portes, décors, toiles de
saltimbanques, enseignes, enluminures populaires ; la littérature démodée,
latin d’église, livres érotiques sans orthographe, *porno de sacristie,* romans de nos aïeules,
contes de fées, *poèmes de salon de coiffure,* petits livres de l’enfance, opéras vieux, refrains niais,
rythmes naïfs, *bottin de téléphone, guide touristique inachevé, mode d'emploi de magnétoscope, codes Microsoft, bilans sanguins.*

Je rêvais croisades, voyages de découvertes dont on n’a pas de relations,
républiques sans histoires, guerres de religion étouffées, révolutions de
mœurs, déplacements de races et de continents : je croyais à tous les
*désenchantements*. *J'étais ko, abîmé. J'avais deux trous rouges au côté droit.*


J’inventai la couleur des voyelles ! --- A noir, E blanc, I rouge, O
bleu, U vert. --- Je réglai la forme et le mouvement de chaque consonne, et,
avec des rythmes instinctifs, je me flattai d’inventer un verbe poétique
accessible, un jour ou l’autre, à tous les sens. Je réservais la traduction.

Ce fut d’abord une étude. J’écrivais des silences, des nuits, je notais
l’inexprimable. Je fixais des vertiges. *J'étais Arthur. J'étais Rimbaud. J'étais Arthur Rimbaud. Je sortais de ma tombe. Je retrouvais ma jambe. Je descendais des fleuves impassibles. J'allais loin, bien loin, comme un bohémien. Je me couchais dans un petit val. Je n'avais que 17 ans. Je m'entêtais affreusement. Je était un autre.*

-------

Loin des oiseaux, des troupeaux, des villageois,\
Que buvais-je, à genoux dans cette bruyère\
Entourée de tendres bois de noisetiers,\
Dans un brouillard d’après-midi tiède et vert !\
\
Que pouvais-je boire dans cette jeune Oise,\
--- Ormeaux sans voix, gazon sans fleurs, ciel couvert ! ---\
Boire à ces gourdes jaunes, loin de ma case\
Chéri ? Quelque liqueur d’or qui fait suer.\
\
Je faisais une louche enseigne d’auberge.\
--- Un orage vint chasser le ciel. Au soir\
L’eau des bois se perdait sur les sables vierges,\
Le vent de Dieu jetait des glaçons aux mares ;\
\
Pleurant, je voyais de l’or --- et ne pus boire. ---

-------

À quatre heures du matin, l’été,\
Le sommeil d’amour dure encore.\
Sous les bocages s’évapore\
L’odeur du soir fêté.\
\
Là-bas, dans leur vaste chantier\
Au soleil des Hespérides,\
Déjà s’agitent --- en bras de chemise ---\
Les Charpentiers.\
\
Dans leurs Déserts de mousse, tranquilles,\
Ils préparent les lambris précieux\
Où la ville\
Peindra de faux cieux.\
\
Ô, pour ces Ouvriers charmants\
Sujets d’un roi de Babylone,\
Vénus ! quitte un instant les Amants\
Dont l’âme est en couronne.\
\
Ô Reine des Bergers,\
Porte aux travailleurs l’eau-de-vie,\
Que leurs forces soient en paix\
En attendant le bain dans la mer à midi.

-------

La vieillerie poétique avait une bonne part dans mon alchimie du verbe.

*Je m’habituais aux hologrammes publicitaires. Je voyais se materialiser devant moi à chaque coin de rue mes désirs les plus profonds
sous la forme de faisceaux lumineux tridimensionels. Un bolide à la place de ma vieille caisse, un palace à la place de mon 2 pièces,
un troisème oeil robotique à la place du canapé clic-clac.*

Puis j’expliquai mes sophismes magiques avec l’hallucination des mots !

Je finis par trouver sacré le désordre de mon esprit. J’étais oisif, en proie
à une lourde fièvre : j’enviais la félicité des bêtes, --- les chenilles, qui
représentent l’innocence des limbes, les taupes, le sommeil de la virginité !


Mon caractère s’aigrissait. Je disais adieu au monde dans d’espèces
de romances :

*Abattre des tours revient à construire des tunnels.*\
\
Qu’il vienne, qu’il vienne,\
Le temps dont on s’éprenne.\
\
J’ai tant fait patience\
Qu’à jamais j’oublie.\
Craintes et souffrances\
Aux cieux sont parties.\
Et la soif malsaine\
Obscurcit mes veines.\
\
Qu’il vienne, qu’il vienne,\
Le temps dont on s’éprenne.\
\
Telle la prairie\
À l’oubli livrée,\
Grandie, et fleurie\
D’encens et d’ivraies,\
Au bourdon farouche\
Des sales mouches.\
\
Qu’il vienne, qu’il vienne,\
Le temps dont on s’éprenne.\
\
J’aimai le désert, les vergers brûlés, les boutiques fanées, les boissons
tiédies. Je me traînais dans les ruelles puantes et, les yeux fermés, je
m’offrais au soleil, dieu de feu.

« Général, s’il reste un vieux canon sur tes remparts en ruines, bombarde-nous
avec des blocs de terre sèche. Aux glaces des magasins splendides ! dans les
salons ! Fais manger sa poussière à la ville. Oxyde les gargouilles. Emplis les
boudoirs de poudre de rubis brûlante... »

Oh ! le moucheron enivré à la pissotière de l’auberge, amoureux de la
bourrache, et que dissout un rayon !

*J'ai faim. Que l'on m'apporte les restes de mon mariage !*\
\
Si j’ai du goût, ce n’est guère\
Que pour la terre et les pierres.\
Je déjeune toujours d’air,\
De roc, de charbons, de fer.\
\
Mes faims, tournez. Paissez, faims,\
Le pré des sons.\
Attirez le gai venin\
Des liserons.\
\
Mangez les cailloux qu’on brise,\
Les vieilles pierres d’églises ;\
Les galets des vieux déluges,\
Pains semés dans les vallées grises.

-------

*J'ai soif d'étreinte. Mangez-moi !*\
\
Le loup criait sous les feuilles\
En crachant les belles plumes\
De son repas de volailles :\
Comme lui je me consume.\
\
Les salades, les fruits\
N’attendent que la cueillette ;\
Mais l’araignée de la haie\
Ne mange que des violettes.\
\
Que je dorme ! que je bouille\
Aux autels de Salomon.\
Le bouillon court sur la rouille,\
Et se mêle au Cédron.\
\
Enfin, ô bonheur, ô raison, j’écartai du ciel l’azur, qui est du noir, et je
vécus, étincelle d’or de la lumière nature. De joie, je prenais une
expression bouffonne et égarée au possible :\
\
Elle est retrouvée !\
Quoi ? l’éternité.\
C’est la mer mêlée\
Au soleil.\
\
Mon âme éternelle,\
Observe ton vœu\
Malgré la nuit seule\
Et le jour en feu.\
\
Donc tu te dégages\
Des humains suffrages,\
Des communs élans !\
Tu voles selon.....\
\
--- Jamais l’espérance.\
Pas d’orietur.\
Science et patience,\
Le supplice est sûr.\
\
Plus de lendemain,\
Braises de satin,\
Votre ardeur\
Est le devoir.\
\
Elle est retrouvée !\
--- Quoi ? --- l’Éternité.\
C’est la mer mêlée\
Au soleil.

-------

*"*Je devins un opéra fabuleux : je vis que tous les êtres ont une fatalité de
bonheur : l’action n’est pas la vie, mais une façon de gâcher quelque force, un
énervement. La morale est la faiblesse de la cervelle*", disait Sappho*.


À chaque être, plusieurs autres vies me semblaient dues. Ce monsieur ne sait
ce qu’il fait : il est un ange. Cette famille est une nichée de chiens. Devant
plusieurs hommes, je causai tout haut avec un moment d’une de leurs autres
vies. --- *"*Ainsi, j’ai aimé un porc*", disait Hildegarde von Bingen*.

*"Les sophismes sont des vices de mouettes", disait Sei Shônagon.*

*"Les mouettes sont vicieuses par nature", disait Marguerite de Navarre.*

*"Aucun de mes vices ne vous concerne", répétait le porc, répétait la mouette.*

Aucun des sophismes de la folie, --- la folie qu’on enferme, --- n’a été oublié
par moi : je pourrais les redire tou*tes*, je tiens le système.

Ma santé fut menacée. La terreur venait. Je tombais dans des sommeils de
plusieurs jours, et, levé, je continuais les rêves les plus tristes. J’étais
mûr pour le trépas, et par une route de dangers ma faiblesse me menait aux
confins du monde et de la Cimmérie, *l'hypertexte* de l’ombre et des tourbillons.

Je dus voyager, distraire les enchantements assemblés sur mon cerveau. Sur la
mer, que j’aimais comme si elle eût dû me laver d’une souillure, je voyais se
lever la croix consolatrice. J’avais été damné par l’arc-en-ciel. Le Bonheur
était ma fatalité, mon remords, mon ver : ma vie serait toujours trop immense
pour être dévouée à la force et à la beauté.

Le Bonheur ! Sa dent, douce à la *&#9760;*, m’avertissait au chant du coq, --- ad
matutinum, au Christus venit, --- dans les plus sombres villes :\
\
Ô saisons, ô châteaux !\
Quelle âme est sans défauts ?\
\
J’ai fait la magique étude\
Du bonheur, qu’aucun n’élude.\
\
Salut à lui, chaque fois\
Que chante le coq gaulois.\
\
Ah ! je n’aurai plus d’envie :\
Il s’est chargé de ma vie.\
\
Ce charme a pris âme et corps\
Et dispersé les efforts.\
\
Ô saisons, ô châteaux !
\
L’heure de sa fuite, hélas !\
Sera l’heure du trépas.\
\
Ô saisons, ô châteaux !

-------

Cela s’est passé. Je sais aujourd’hui saluer la beauté.

*Les pavés ont la forme des crânes qu'ils épousent.*

Ah ! cette vie de mon enfance, la grande route par tous les temps, sobre
surnaturellement, plus désintéressé que le meilleur des mendiants, fier de
n’avoir ni pays, ni amis, quelle sottise c’était. --- Et je m’en aperçois
seulement !

*Les crânes deviennent pavés à force d'être piétinés.*

--- J’ai eu raison de mépriser ces bonshommes qui ne perdraient pas l’occasion
d’une caresse, parasites de la propreté et de la santé des femmes,
aujourd’hui qu’elles sont si peu d’accord.

J’ai eu raison dans tous mes dédains : puisque je m’évade !

Je m’évade ! *Je t'évade!*

Je m’explique. *Je t'explique*

Hier encore, je soupirais : « Ciel ! Sommes-nous assez de damnés ici-bas ! Moi
j’ai tant de temps déjà dans leur troupe ! Je les connais tous. Nous nous
reconnaissons toujours ; nous nous dégoûtons. La charité nous est inconnue.
Mais nous sommes polis ; nos relations avec le monde sont très-convenables.
» Est-ce étonnant ? Le monde ! les marchands, les naïfs ! --- Nous ne sommes
pas déshonorés. --- Mais les élus, comment nous recevraient-ils ? Or il y a des
gens hargneux et joyeux, de faux élus, puisqu’il nous faut de l’audace ou de
l’humilité pour les aborder. Ce sont les seuls élus. Ce ne sont pas des
bénisseurs !

M’étant retrouvé deux sous de raison --- ça passe vite ! --- je vois que mes
malaises viennent de ne m’être pas figuré assez tôt que nous sommes
à l’Occident. Les marais occidentaux ! Non que je croie la lumière altérée, la
forme exténuée, le mouvement égaré... Bon ! voici que mon esprit veut
absolument se charger de tous les développements cruels qu’a subis l’esprit
depuis la fin de l’Orient... Il en veut, mon esprit !

... Mes deux sous de raison sont finis ! --- L’esprit est autorité, il veut que
je sois en Occident. *Pour conclure comme je voulais taire le faire il faudrait*.

J’envoyais au diable les palmes des martyrs, les rayons de l’art, l’orgueil des
inventeurs, l’ardeur des pillards ; je retournais à l’Orient et à la sagesse
première et éternelle. --- Il paraît que c’est un rêve de paresse grossière !

Pourtant, je ne songeais guère au plaisir d’échapper aux souffrances modernes.
Je n’avais pas en vue la sagesse bâtarde du Coran. --- Mais n’y a-t-il pas un
supplice réel en ce que, depuis cette déclaration de la science, le
christianisme, l’homme se joue, se prouve les évidences, se gonfle du plaisir
de répéter ces preuves, et ne vit que comme cela ! Torture subtile, niaise ;
source de mes divagations spirituelles. La nature pourrait s’ennuyer,
peut-être ! M. Prudhomme est né avec le Christ.

N’est-ce pas parce que nous cultivons la brume ! Nous mangeons la fièvre avec
nos légumes aqueux. Et l’ivrognerie ! et le tabac ! et l’ignorance ! et les
dévouements ! --- Tout cela est-il assez loin de la pensée de la sagesse de
l’Orient, le *rhizome primitif* ? Pourquoi un monde moderne, si de pareils
poisons s’inventent !


Les gens d’Église diront : C’est compris. Mais vous voulez parler de l’Éden.
Rien pour vous dans l’histoire des peuples orientaux. --- C’est vrai ; c’est
à l’Éden que je songeais ! Qu’est-ce que c’est pour mon rêve, cette pureté des
races antiques !

Les philosophes*ses* : Le monde n’a pas d’âge. L’humanité se déplace, simplement.
Vous êtes en Occident, mais libre d’habiter dans votre Orient, quelque ancien
qu’il vous le faille, --- et d’y habiter bien. Ne soyez pas un vaincu.
Philosophes*ses*, vous êtes de votre Occident.

Mon esprit, prends garde. Pas de partis de salut violents. Exerce-toi !
--- Ah ! la science ne va pas assez vite pour nous !

--- Mais je m’aperçois que mon esprit dort.

S’il était bien éveillé toujours à partir de ce moment, nous serions bientôt
à la vérité, qui peut-être nous entoure avec ses anges pleurant !... --- S’il
avait été éveillé jusqu’à ce moment-ci, c’est que je n’aurais pas cédé aux
instincts délétères, à une époque immémoriale !... --- S’il avait toujours été
bien éveillé, je voguerais en pleine sagesse !...

Ô pureté ! pureté !

*Qui de pureté la vision m’a donné d’éveil cette minute c’est* ! --- Par
l’esprit on va à Dieu !

Déchirante infortune !

*Je lèche des éclairs en attendant l'aurore.*

Le travail humain ! c’est l’explosion qui éclaire mon abîme de temps en temps.

*Je lèche l'aurore en attendant les éclaires.*

« Rien n’est vanité ; à la science, et en avant ! » crie l’Ecclésiaste moderne,
c’est-à-dire Tout le monde. Et pourtant les cadavres des méchants et des
fainéants tombent sur le cœur des autres... Ah ! vite, vite un peu ;
là-bas, par delà la nuit, ces récompenses futures, éternelles...
les échappons-nous ?...

--- Qu’y puis-je ? Je connais le travail ; et la science est trop lente. Que la
prière galope et que la lumière gronde... je le vois bien. C’est trop simple,
et il fait trop chaud ; on se passera de moi. J’ai mon devoir, j’en serai fier
à la façon de plusieurs, en le mettant de côté.

*Mon code a glitché.* Allons ! feignons, fainéantons, ô pitié ! Et nous existerons
en nous amusant, en rêvant amours monstres et univers fantastiques, en nous
plaignant et en querellant les apparences du monde, saltimbanque, mendiant,
artiste, bandit, --- prêtre ! *Dans l’interstice hypertextuel, les sens me manquent* ; gardien des aromates sacrés, confesseur, martyr...

Je reconnais là ma sale éducation d’enfance. Puis quoi !... Aller mes vingt
ans, si les autres vont vingt ans...

Non ! non ! à présent je me révolte contre la *&#9760;* ! Le travail paraît trop
léger à mon orgueil : ma trahison au monde serait un supplice trop court. Au
dernier moment, j’attaquerais à droite, à gauche...

Alors, --- oh ! --- chère pauvre âme, l’éternité serait-elle pas perdue pour
nous !

*La cendre mâtine ma diane.*

N’eus-je pas une fois une jeunesse aimable, héroïque, fabuleuse, à écrire sur
des feuilles d’or, --- trop de chance ! Par quel crime, par quelle erreur,
ai-je mérité ma faiblesse actuelle ? Vous qui prétendez que des bêtes poussent
des sanglots *longs* de chagrin, que des malades désespèrent, que des morts rêvent mal,
tâchez de raconter ma chute et mon sommeil. Moi, je ne puis pas plus
m’expliquer que le mendiant avec ses continuels Pater et Ave Maria. Je ne
sais plus parler ! *Sais-je encore écrire ?*

Pourtant, aujourd’hui, je crois avoir fini la relation de mon enfer. C’était
bien l’enfer ; l’ancien, celui dont le fils de l’homme ouvrit les portes.

Du même désert, à la même nuit, toujours mes yeux las se réveillent à l’étoile
d’argent, toujours, sans que s’émeuvent les Rois de la vie, les trois mages, le
cœur, l’âme, l’esprit. Quand irons-nous, par delà les grèves et les monts,
saluer la naissance du travail nouveau, la sagesse nouvelle, la fuite des
tyrans et des démons, la fin de la superstition, adorer --- les premiers ! ---
Noël sur la terre !

Le chant des cieux, la marche des peuples ! Esclaves, ne maudissons pas la vie.

*Nos bonnes consciences saliront nos renaissances à la pureté.*

L’automne déjà ! --- Mais pourquoi regretter un éternel soleil, si nous sommes
engagés à la découverte de la clarté divine, --- loin des gens qui meurent sur
les saisons.

*La pureté viendra à bout de nos bonnes consciences.*

L’automne. Notre barque élevée dans les brumes immobiles tourne vers le port de
la misère, la cité énorme au ciel taché de feu et de boue. Ah ! les haillons
pourris, le pain trempé de pluie, l’ivresse, les mille amours qui m’ont
crucifié ! Elle ne finira donc point cette goule reine de millions d’âmes et de
corps morts et qui seront jugés ! Je me revois la peau rongée par la boue et
la peste, des vers plein les cheveux et les aisselles et encore de plus gros
vers dans le cœur, étendu parmi les inconnus sans âge, sans sentiment...
J’aurais pu y mourir... L’affreuse évocation ! J’exècre la misère.

*Et parce que du comfort la saison c’est l’hiver je redoute* !

--- Quelquefois je vois au ciel des plages sans fin couvertes de blanches
nations en joie. Un grand vaisseau d’or, au-dessus de moi, agite ses pavillons
multicolores sous les brises du matin. J’ai créé toutes les fêtes, tous les
triomphes, tous les drames. J’ai essayé d’inventer de nouvelles fleurs, de
nouveaux astres, de nouvelles chairs, de nouvelles langues. J’ai cru acquérir
des pouvoirs surnaturels. Eh bien ! je dois enterrer mon imagination et mes
souvenirs ! Une belle gloire d’artiste et de conteur emportée !

Moi ! moi qui me suis dit *0 ou 1*, dispensé de toute morale, je suis rendu
au sol, avec un devoir à chercher, et la réalité rugueuse à étreindre !
Paysan !

Suis-je trompé ? la charité serait-elle sœur de la *&#9760;*, pour moi ?

Enfin, je demanderai pardon pour m’être nourri de mensonge. Et allons.

Mais pas une main amie ! et où puiser le secours ?

-------

*Idiot. Un soir, je dus voyager. Ainsi, j'en prenais l'habitude. Vivre somnambule. Je croyais à tous les enchantements. Cette bouche sous tes yeux. Nous nous accordions. N'est-ce pas parce que nous cultivions la brume ? L'automne déjà. Je crois avoir fini. C'était bien l'enfer. Esclaves, ne maudissons pas la vie. Il faut être absolument moderne. Ou idiot. Horreur de ma bêtise. L'heure nouvelle. Une main amie. Une main de plume. Une autre de charrue. Idiot. Un soir, je dus voyager. J'avais entrevu. Entrevu : faim, soif, cris, danse, par des nuits d'hiver. Quitter ce continent, quitter la vision, elle. Vivre somnambule. Feu sur moi. Je meurs de soif. Je m'y habituerai. Je suis faible. L'idiot qui a perdu la sagesse. Veut-on des chants ? Des danses ? Elle. Le désert. Tout de suite, je suis tombé. Jamais jaloux. Nous nous accordions. J'aimais, je rêvais, j'inventai. J'écrivais des silences, des nuits, fixais des vertiges. L'odeur du soir fêté. Idiot. Un soir, je dus voyager. Distraire les enchantements assemblés sur mon cerveau. L'eau des bois se perdait sur les sables vierges. J'aimai le désert. Elle, était ma fatalité, ma vie serait toujours trop immense pour être dévouée à la force et la beauté. J'étais mûr. Idiot.*

Oui l’heure nouvelle est au moins très-sévère.

Car je puis dire que la victoire m’est acquise : les grincements de dents, les
sifflements de feu, les soupirs empestés se modèrent. Tous les souvenirs
immondes s’effacent. Mes derniers regrets détalent, --- des jalousies pour les
mendiants, les brigands, les amis de la *&#9760;*, les *arrière-gardes* de toutes sortes.
--- Damnés, si je me vengeais !

Il *nous* faut être *absolu, absous, obstru,* moderne *pour nos contemporains*.

Point de cantiques : tenir le pas gagné. Dure nuit ! le sang séché fume sur *nos
faces*, et *nous n'avons plus* rien derrière *nous*, que cet horrible arbrisseau !... Le combat
spirituel est aussi brutal que la bataille d’hommes ; mais la vision de la
justice est le plaisir de Dieu seul.

Cependant c’est la veille *qui nous nuit*. Recevons tous les influx de vigueur et de tendresse
réelle. Et à l’aurore, armés d’une *patiente ardence*, nous entrerons *révoltants* aux
splendides villes. *Sans citoyenneté, bras à bras, chargés de nous-mêmes, du poids de nos remparts, des transferts de nos blocages, nous remplacerons de notre souffle corrosif l'air vicié d'or et de marbre.*

Que *parlions-nous* de main amie ! Un bel avantage, c’est que *l'on* puis rire des
vieilles amours mensongères, et frapper de honte ces couples menteurs, --- *Nous vîmes*
l’enfer des femmes là-bas ; --- et il *nous* sera loisible de posséder la
vérité dans une âme et un corps.

*Ci-GIT je*

avril-août, *3073*.
